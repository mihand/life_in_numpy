#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from setuptools import setup

if __name__ == "__main__":
    setup(
        name='lifedemos',
        version='0.1',
        package_dir={'': 'src'},
        install_requires=[
            'numpy',
            'pillow',
            'pyglet'
        ],
        entry_points={
            'console_scripts': [
                'life_in_numpy=life.text_ui:main'
            ]
        },
    )


