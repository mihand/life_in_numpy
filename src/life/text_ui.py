import life.pygletui
import life.grey
import life.conway
import life.qtui
import argparse

def main():
    p = argparse.ArgumentParser()
    p.add_argument('--gui', choices=['qt', 'pyglet'], default='pyglet')
    p.add_argument('--sim', choices=['grey', 'life'], default='grey')
    p.add_argument('--no-numpy', action='store_true')
    #args = p.parse_args(['--gui=qt', '--sim=life'])
    #args = p.parse_args(['--gui=pyglet', '--sim=life'])
    args = p.parse_args()

    ui_main = {
        'qt': life.qtui.main,
        'pyglet': life.pygletui.main
    }[args.gui]
    sim_class = {
        ('grey', True): life.grey.GreyScott,
        ('life', True): life.conway.Conway
    }[args.sim, not args.no_numpy]

    h = 168
    sim = sim_class(grid_size=(h, int(h*16/9)))
    sim.rand()
    sim.stimulate(10, 10, 20)
    ui_main(sim)

if __name__ == '__main__':
    main()