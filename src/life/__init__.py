# Exemplu Conway's game of life
# =============================
# Game of life e un automat celular.
# Anumite reguli se executa pentru fiecare pozitie dintr-un grid
# O pozitie poate fi vie sau moarta.
# . . . .
# . . o .
# . . . o
# . o o o
# O celula se naste traieste si moare in functie de cati din vecini sunt vii
# v v v   <-- vecinii
# v . v
# v v v
# Any live cell with fewer than two live neighbours dies
# Any live cell with two or three live neighbours lives
# Any live cell with more than three live neighbours dies
# Any dead cell with exactly three live neighbours becomes a live cell