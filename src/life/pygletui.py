import os
import pyglet
from pyglet import gl
import numpy as np
from PIL import Image

PALETTE_RdGy = 'RdGy.png'
PALETTE_RdYlBu = 'RdYlBu.png'
PALETTE_YlGnBu = 'YlGnBu.png'

def palette_from_image(palette_name):
    im = Image.open(os.path.join(os.path.dirname(__file__), palette_name))
    if im.mode != 'RGB':
        raise ValueError('bad palette image')
    im = np.asarray(im, dtype=np.uint8)
    return im[0, ...]


palette = None


def imshow(a):
    """
    transforms the 2d array a into a 3d array replacing each value
    with a color from a palette
    """
    if len(a.shape) != 2:
        raise ValueError('imshow needs a 2d array not %s' % a.shape)
    # normalize to 0..len(palette)
    normalized = (a - a.min())/(a.max() - a.min()) * (len(palette) - 1)
    # need integers to index
    normalized = normalized.astype(np.int)
    return palette[normalized]


class RdWindow(pyglet.window.Window):
    def __init__(self, simulator):
        super().__init__(resizable=True)

        self.simulator = simulator
        self.iterations = 0

        self.fps_display = pyglet.clock.ClockDisplay()
        self.info_label = pyglet.text.Label('a = 0')

        data = next(self.simulator)
        pic = imshow(data)

        self.image = pyglet.image.ImageData(pic.shape[1], pic.shape[0],
                                            'RGB', pic.tobytes())
        #self.image.anchor_x = self.image.width//2
        #self.image.anchor_y = self.image.height//2

        gl.glEnable(gl.GL_TEXTURE_2D)

    def on_draw(self):
        self.clear()
        tex = self.image.get_texture()
        # nearest interpolation for textures
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
        tex.blit(0, 0, width=self.image.width * 6, height=self.image.height * 6)
        self.fps_display.draw()
        self.info_label.text = f'iterations = {self.iterations}'
        self.info_label.draw()

    def on_tick(self, dt):
        self.iterations += 1
        data = next(self.simulator)
        pic = imshow(data)
        self.image.data = pic.tobytes()


def main(simulator, palette_name=PALETTE_YlGnBu):
    global palette
    palette = palette_from_image(palette_name)
    window = RdWindow(simulator)
    pyglet.clock.schedule_interval(window.on_tick, 1/30)
    pyglet.app.run()

