import numpy as np


class Conway:
    def __init__(self, grid_size=(128, 128)):
        self.data = np.zeros(grid_size, dtype=np.int8)

    def rand(self):
        w, h = self.data.shape
        self.data[1:-1, 1:-1] = np.random.rand(w - 2, h - 2) > 0.5

    def stimulate(self, x, y, w):
        self.data[x:x+w, y:y+w] = np.random.rand(w, w) > 0.5

    def __next__(self):
        pop = self.neighbour_population()
        cell = self.data[1:-1, 1:-1]
        #cell and population in [2, 3] or not cell and population == 3
        self.data[1:-1, 1:-1] = np.where( cell > 0,
                 (pop == 2) | (pop == 3),
                 pop == 3
        )
        return self.data


    def neighbour_population(self):
        left = self.data[0:-2, 1:-1]
        right = self.data[2:, 1:-1]
        up = self.data[1:-1, 0:-2]
        down = self.data[1:-1, 2:]

        left_up = self.data[0:-2, 0:-2]
        right_up = self.data[2:, 0:-2]
        right_down = self.data[2:, 2:]
        left_down = self.data[0:-2, 2:]

        return left + right + up + down + left_up + left_down + right_down + right_up

