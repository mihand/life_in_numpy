import numpy as np

class Sim:
    MULTISTEP = 4
    dt = 0.9
    dx = 1.0

    def __init__(self, grid_size=(256, 256)):
        self.w, self.h = grid_size
        self.data = np.zeros(grid_size)

    def laplacian(self):
        "laplacian for the whole center grid vectorized"
        left = self.data[0:-2, 1:-1]
        right = self.data[2:, 1:-1]
        up = self.data[1:-1, 0:-2]
        down = self.data[1:-1, 2:]
        center = self.data[1:-1, 1:-1]
        return (left + right + up + down + center * (-4.0)) / self.dx ** 2

    def __next__(self):
        # do MULTISTEP integration steps. Simple euler scheme
        for _ in range(self.MULTISTEP):
            df = self.dfun()
            self.data[1:-1, 1:-1] = self.data[1:-1, 1:-1] + self.dt * df

        # sanity checks
        if not df.any():
            raise ValueError('zero derivatives')
        if self.data.max() - self.data.min() > 1e4:
            raise ValueError('diverged')
        return self.project()

    def project(self):
        return self.data


class GreyScott(Sim):
    D_v = 0.041
    k = 0.06
    D_u = 0.082
    F = 0.035
    dt = 0.9
    dx = 1.0
    MULTISTEP = 16

    def __init__(self, grid_size=(256, 256)):
        super().__init__(grid_size)
        self.data = np.zeros(grid_size + (2, ))
        self.data[:, :, 0] = 1
        self.data[:, :, 1] = 0
        # just to avoid zero derivatives
        self.stimulate(0, 0, 2)

    def rand(self):
        self.data[:, :, 0] = 1 - np.random.rand(self.w, self.h) * 0.5
        self.data[:, :, 1] = 0 + np.random.rand(self.w, self.h) * 0.25

    def dfun(self):
        " the derivative of the system "
        u = self.data[1:-1, 1:-1, 0]
        v = self.data[1:-1, 1:-1, 1]
        laplacian = self.laplacian()
        ret = np.zeros((self.w - 2, self.h - 2, 2))
        ret[:, :, 0] = self.D_u * laplacian[:, :, 0] - u * v ** 2 + self.F * (1 - u)
        ret[:, :, 1] = self.D_v * laplacian[:, :, 1] + u * v ** 2 - (self.F + self.k) * v
        return ret

    def stimulate(self, x, y, w):
        self.data[x:x+w, y:y+w, 0] = 0.5
        self.data[x:x+w, y:y+w, 1] = 0.25

    def project(self):
        " return a 2d projection of the state space for plotting "
        return self.data[:, :, 0]

