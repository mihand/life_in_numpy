import sys
from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg


class Window(QtWidgets.QWidget):
    def __init__(self, simulator):
        super().__init__()
        self.build_ui()
        self.simulator = simulator

    def build_ui(self):
        self.setObjectName("Form")
        self.resize(800, 600)

        self.im_widget = pg.ImageView(self)

        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(self.im_widget)

        self.count = 0
        self.im_widget.show()

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.on_tick)

    def start_timers(self):
        self.timer.start(32)

    def on_tick(self):
        self.count = self.count + 1
        try:
            data = next(self.simulator)
        except Exception:
            self.timer.stop()
            raise
        self.im_widget.setImage(data)

    def closeEvent(self, event):
        self.timer.stop()


def main(simulator):
    app = QtWidgets.QApplication(sys.argv)
    form = Window(simulator)
    form.start_timers()
    form.show()
    sys.exit(app.exec_())

